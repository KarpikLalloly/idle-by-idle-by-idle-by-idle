using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerEnemyInteractor : MonoBehaviour
{
    [SerializeField] private EnemyInteractor _enemyInteractor;
    private Player _player;

    private void OnEnable()
    {
        _player = GetComponent<Player>();
        _enemyInteractor.OnEnemyContact += _player.OnEnemyInteract;
    }

    private void OnDisable()
    {
        _enemyInteractor.OnEnemyContact -= _player.OnEnemyInteract;
    }
}
