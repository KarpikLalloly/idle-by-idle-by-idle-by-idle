using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    public event Action<float> ScoreUpdated;

    private float _coins;

    public void AddCoin()
    {
        _coins++;
        ScoreUpdated?.Invoke(_coins);
    }

    public void OnEnemyInteract(Enemy enemy)
    {
        AddCoin();
        Destroy(enemy.gameObject);
    }
}
