using UnityEngine;

[RequireComponent(typeof(Pickup))]
public class Coin : MonoBehaviour
{
    private Pickup _pickup;

    private void OnEnable()
    {
        _pickup = GetComponent<Pickup>();
        _pickup.Entered += OnPickupEntered;
    }

    private void OnDisable()
    {
        _pickup.Entered -= OnPickupEntered;
    }

    private void OnPickupEntered(Collider2D collider)
    {
        if (!collider.TryGetComponent(out Player player))
        {
            return;
        }

        player.AddCoin();
        Destroy(gameObject);
    }
}
