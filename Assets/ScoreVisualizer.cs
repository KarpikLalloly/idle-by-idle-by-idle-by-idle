using UnityEngine;
using UnityEngine.UIElements;

public class ScoreVisualizer : MonoBehaviour
{
    [SerializeField] private string _labelName;
    [SerializeField] private UIDocument _ui;

    private Label _scoreLabel;

    private Player _player;

    private void OnEnable()
    {
        _scoreLabel = _ui.rootVisualElement.Q<Label>(_labelName);

        _player = FindAnyObjectByType<Player>();
        _player.ScoreUpdated += UpdateScore;
    }

    private void OnDisable()
    {
        _player.ScoreUpdated -= UpdateScore;
    }

    private void UpdateScore(float score)
    {
        _scoreLabel.text = score.ToString();
    }
}
