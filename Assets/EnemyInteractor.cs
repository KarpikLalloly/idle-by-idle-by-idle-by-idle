using System;
using UnityEngine;

public class EnemyInteractor : MonoBehaviour
{
    public event Action<Enemy> OnEnemyContact;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Enemy enemy))
        {
            OnEnemyContact?.Invoke(enemy);
        }
    }
}
